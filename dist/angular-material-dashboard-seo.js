/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardSeo', [ //
'ngMaterialDashboard', 'seen-seo'// 
]);

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardSeo')
/**
 * 
 */
.config(function ($routeProvider) {
    $routeProvider
    /**
     * @ngdoc ngRoute
     * @name /seo/backends
     * @description List of SEO backends
     * 
     */
    .when('/seo/backends', {
        controller: 'AmdSeoBackendsCtrl',
        controllerAs: 'ctrl',
        templateUrl: 'views/amd-seo-backends.html',
        navigate: true,
        groups: ['seo'],
        name: 'Prerender backends',
        icon: 'dvr',
        /*
         * @ngInject
         */
        protect: function ($rootScope) {
            return !$rootScope.app.user.tenant_owner;
        }
    })
    /**
     * @ngdoc ngRoute
     * @name /seo/backends/new
     * @description Adding new SEO backend
     * 
     */
    .when('/seo/backends/new', {
        controller: 'AmdSeoBackendNewCtrl',
        controllerAs: 'ctrl',
        templateUrl: 'views/amd-seo-backend-new.html',
        navigate: true,
        /*
         * @ngInject
         */
        protect: function ($rootScope) {
            return !$rootScope.app.user.tenant_owner;
        },
        groups: ['seo'],
        name: 'New prerender backend',
        icon: 'add'
    })
    /**
     * @ngdoc ngRoute
     * @name /seo/backends/:id
     * @description List of seo backend
     * 
     */
    .when('/seo/backends/:id', {
        controller: 'AmdSeoBackendCtrl',
        controllerAs: 'ctrl',
        templateUrl: 'views/amd-seo-backend.html',
        /*
         * @ngInject
         */
        protect: function ($rootScope) {
            return !$rootScope.app.user.tenant_owner;
        }
    })

    /**
     * @ngdoc ngRoute
     * @name /seo/links
     * @description List of links
     * 
     */
    .when('/seo/links', {
        controller: 'AmdSeoLinksCtrl',
        controllerAs: 'ctrl',
        templateUrl: 'views/amd-seo-links.html',
        navigate: true,
        /*
         * @ngInject
         */
        protect: function ($rootScope) {
            return !$rootScope.app.user.tenant_owner;
        },
        groups: ['seo'],
        name: 'Sitemap links',
        icon: 'link'
    })
    /**
     * @ngdoc ngRoute
     * @name /seo/links/new
     * @description Adding new link to SEO
     * 
     * Links are used in seo engine, site map generator and etc.
     * 
     */
    .when('/seo/links/new', {
        controller: 'AmdSeoLinkNewCtrl',
        controllerAs: 'ctrl',
        templateUrl: 'views/amd-seo-link-new.html',
        navigate: true,
        /*
         * @ngInject
         */
        protect: function ($rootScope) {
            return !$rootScope.app.user.tenant_owner;
        },
        groups: ['seo'],
        name: 'New sitemap link',
        icon: 'add'
    })
    /**
     * @ngdoc ngRoute
     * @name /seo/links/:id
     * @description Link details
     * 
     */
    .when('/seo/links/:id', {
        controller: 'AmdSeoLinkCtrl',
        controllerAs: 'ctrl',
        templateUrl: 'views/amd-seo-link.html',
        /*
         * @ngInject
         */
        protect: function ($rootScope) {
            return !$rootScope.app.user.tenant_owner;
        }
    })
    /**
     * @ngdoc ngRoute
     * @name /seo/sitemap
     * @description seo sitemap
     * 
     */
    .when('/seo/sitemap', {
        templateUrl: 'views/sitemap.html',
        navigate: true,
        /*
         * @ngInject
         */
        protect: function ($rootScope) {
            return !$rootScope.app.user.tenant_owner;
        },
        groups: ['seo'],
        name: 'View Sitemap',
        icon: 'public'
    })
    /**
     * @ngdoc ngRoute
     * @name /seo/crawled-links
     * @description List of links visited by crawlers
     * 
     */
    .when('/seo/crawled-links', {
        templateUrl: 'views/amd-seo-crawled-links.html',
        controller: 'AmdSeoCrawledLinksCtrl',
        controllerAs: 'ctrl',
        navigate: true,
        /*
         * @ngInject
         */
        protect: function ($rootScope) {
            return !$rootScope.app.user.tenant_owner;
        },
        groups: ['seo'],
        name: 'Crawled links',
        icon: 'public'
    })
    /**
     * @ngdoc ngRoute
     * @name /seo/crawled-links/:crawledLinkId/render
     * @description A render of a crawled link in a page
     * 
     */
    .when('/seo/crawled-links/:crawledLinkId/render', {
        templateUrl: 'views/amd-seo-content-render.html',
//      controller: 'AmdSeoContentRenderCtrl',
//      controllerAs: 'ctrl'
    });
});
'use strict';
angular.module('ngMaterialDashboardSeo')

/**
 * @ngdoc controller
 * @name AmdSeoBackendCtrl
 * @description
 *  # AmdSeoBackendCtrl Controller of the ngMaterialDashboardSeo
 */
.controller('AmdSeoBackendNewCtrl', function ($scope, $seo, $navigator) {
	var ctrl ={
			status: 'relax'
	};

	function loadEngines()
	{
		return $seo.getEngines()//
		.then(function(engines){
			$scope.engines = engines;
		});
	}

	function loadBackendProperties(engine){
	    $scope.selectedEngine = engine;
		return $seo.getEngine(engine.id)//
		.then(function(properties){
			$scope.properties = properties.children;
		});
	}

	function newBackend(engine, data){
		ctrl.status = 'working';
		data.type = engine.type;
		return $seo.putBackend(data)//
		.then(function(backend){
			$navigator.openPage('seo/backends/'+backend.id);
		}, function(){
			alert('Failed to create backend');
		})//
		.finally(function(){
			ctrl.status = 'relax';
		});
	}

	$scope.$watch('_engine', function(newValue){
		if(newValue){
			return loadBackendProperties(newValue);
		}
	});

	$scope.ctrl = ctrl;
	$scope.loadEngines = loadEngines;
	$scope.newBackend = newBackend;
	$scope._userValus = {};
});

'use strict';
angular.module('ngMaterialDashboardSeo')

/**
 * @ngdoc controller
 * @name AmdSeoBackendCtrl
 * @description Backend controller
 * 
 * Manages a backend view
 */
.controller('AmdSeoBackendCtrl', function ($scope, $seo, $routeParams, $navigator) {

	/**
     * Remove backend
     * 
     * @memberof AmdSeoBackendCtrl
     * @return {promiss} to remove the backend
     */
	function remove() {
	    if($scope.loadingBackend) {
	        return;
	    }
		confirm('The backend will be deleted. There is no undo action.')//
		.then(function(){
			return $scope.loadingBackend = $scope.backend.delete()//
			.then(function(){
                $navigator.openPage('seo/backends');
            }, function(){
                alert('Fail to delete backend.');
            });
		})//
		.finally(function(){
		    $scope.loadingBackend = false;
		});
	}
	
	/**
     * Save changes
     * 
     * Save all changed of the current backend
     * 
     * @memberof AmdSeoBackendCtrl
     * @return {promiss} to save the backend
     */
	function save(){
		if($scope.loadingBackend){
			return;
		}
		return $scope.loadingBackend = $scope.backend.update()//
		.then(function(){
			toast('Backend is saved');
		})//
		.finally(function(){
		    $scope.loadingBackend = false;
		});
	}

	/**
     * Load the backend
     * 
     * @memberof AmdSeoBackendCtrl
     * @return {promiss} to load the backend
     */
	function load() {
	    if($scope.loadingBackend){
	        return;
	    }
	    $scope.loadingBackend = true;
		return $seo.getBackend($routeParams.id)//
		.then(function(backend){
			$scope.backend = backend;
		})
		.finally(function(){
		    $scope.loadingBackend = false;
		});
	}

	$scope.remove = remove;
	$scope.save = save;
	load();
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardSeo')

	/**
	 * @ngdoc controller
	 * @name AmdSeoBackendsCtrl
	 * @description # AmdSeoBackendsCtrl Controller of the ngMaterialDashboardSeo
	 */
	.controller('AmdSeoBackendsCtrl', function ($scope, $navigator, $seo, $controller) {

	    angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
		$scope: $scope
	    }));

	    // Overried the function
	    this.getModelSchema = function () {
		return $seo.backendSchema();
	    };

	    // get groups
	    this.getModels = function (parameterQuery) {
		return $seo.getBackends(parameterQuery);
	    };

	    // get a group
	    this.getModel = function (id) {
		return $seo.getBackend(id);
	    };

	    // Add group
	    this.addModel = function (model) {
		return $seo.putBackend(model);
	    };

	    // delete group
	    this.deleteModel = function (model) {
		return $seo.deleteBackend(model.id);
	    };

	    this.init({
		eventType: '/seo/backends'
	    });

	    /**
	     * To add a new backend
	     * 
	     * @returns
	     */
	    this.addBackend = function () {
		$navigator.openPage('seo/backends/new');
	    };

	    var ctrl = this;
	    this.addActions([{
		    title: 'New backend',
		    icon: 'add',
		    action: function () {
			ctrl.addBackend();
		    }
		}]);

	});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngMaterialDashboardSeo')

/**
 * @ngdoc controller
 * @name AmdSeoContentRenderCtrl
 * @description Content render controller
 * 
 * Shows content preview and render the page
 */
.controller('AmdSeoContentRenderCtrl', function ($scope, $element, $routeParams, $sce, $seoContent, $seo) {
    // controller attributes
    /*
     * loading, 404, 500
     */
    this.status = 'loading';
    this.seoContent = null;
    $scope.options = {
            removeAngularjsMDTags: true,
            removeAngularjsTags: true,
            removeStyle: true
    };
    $scope.editable = true;

    /**
     * Load SEO Content
     */
    this.loadSeoContent = function (contetnId) {
        var ctrl = this;
        $seo.getContent(contetnId)
        .then(function(content){
            ctrl.seoContent = content;
            // Load secure path
            ctrl.currentContenttUrl = $sce.trustAsResourceUrl(ctrl.seoContent.url);
        });
    };

    /**
     * Creates and upload the preview of the page
     */
    this.uploadPreview = function(options){
        var ctrl = this;
        $seoContent.optimize(this.getSeoContentHtml(), options)
        .then(function(optimizedHtml){
            return ctrl.seoContent.uploadValue(optimizedHtml);
        })//
        .then(function(newSeoContent){
            ctrl.seoContent = newSeoContent;
        });
    };

    this.getSeoContentHtml = function(){
        var iframe = $element.find('iframe')[0];

        var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
        var html = new XMLSerializer().serializeToString(innerDoc);

        return html;
    };
    
    this.saveContent = function(){
        return this.seoContent.update();
    };

    // load controller
    if ($routeParams.crawledLinkId) {
        this.loadSeoContent($routeParams.crawledLinkId);
    } else {
        this.status = '404';
    }
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardSeo')

/**
 * @ngdoc controller
 * @name AmdSeoCrawledLinksCtrl
 * @description # Controller of the crawled links
 */
.controller('AmdSeoCrawledLinksCtrl', function ($scope, $navigator, $translate, $seo, $controller) {

    // Extends with ItemsController
    angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
        $scope: $scope
    }));

    /*
     * Override the function
     */
    this.getModelSchema = function () {
        return $seo.linkSchema();
    };

    // get crawled links
    this.getModels = function (parameterQuery) {
        return $seo.getContents(parameterQuery);
    };

    // get a crawled link
    this.getModel = function (id) {
        return $seo.getContent(id);
    };

    // delete crawled link
    this.deleteModel = function (item) {
        return item.delete();
    };

    this.init({
        eventType: '/seo/crawled-links'
    });

    /**
     * Upload content to pbobject
     * 
     * @param pobject
     */
    this.upload = function (pobject) {
        $navigator.openDialog({
            templateUrl: 'views/dialogs/select-file.html',
            config: {
                _files: []
            }
        })//
        .then(function (config) {
            return pobject.uploadValue(config.files[0].lfFile);//
        }, function () {})
        .then(function (content) {
            //TODO: Masood, 2019: Do works if needed
        }, function () {
            alert($translate.instant('Failed to upload content'));
        });
    };
});
'use strict';
angular.module('ngMaterialDashboardSeo')

/**
 * @ngdoc controller
 * @name AmdSeoEngineCtrl
 * @description 
 * 
 * # AmdSeoEngineCtrl Controller of the ngMaterialDashboardSeo
 */
.controller('AmdSeoEngineCtrl', function ($scope) {

});

'use strict';
angular.module('ngMaterialDashboardSeo')

/**
 * @ngdoc controller
 * @name AmdSeoEnginesCtrl
 * @description
 *
 * # AmdSeoEnginesCtrl Controller of the ngMaterialDashboardSeo
 */
.controller('AmdSeoEnginesCtrl', function ($scope) {

});

'use strict';
angular.module('ngMaterialDashboardSeo')

/**
 * @ngdoc controller
 * @name AmdSeoLinkCtrl
 * @description
 *  # AmdSeoLinkCtrl Controller of the ngMaterialDashboardSeo
 */
.controller('AmdSeoLinkNewCtrl', function ($scope, $seo, $routeParams, $navigator) {

	var ctrl = {
		status: 'relax'
	};

	function addLink(model) {
		ctrl.status = 'working';
		$seo.putLink(model)//
		.then(function(link){
			$navigator.openPage('seo/links/'+link.id);
		}, function(){
			alert('failed to add a new link.');
		})//
		.finally(function(){
			ctrl.status = 'relax';
		});
	}

	function cancel(){
		$navigator.openPage('seo/links');
	}
	
	function formatDate(date) {
	    return moment(date).format('YYYY-MM-DD');
	}
	
	$scope.formatDate = formatDate;
	$scope.changeFrequencies = [{
	    value: 'always',
	    title: 'Always'
	}, {
	    value: 'hourly',
	    title: 'Hourly'
	}, {
	    value: 'daily',
	    title: 'Daily'
	}, {
	    value: 'weekly',
	    title: 'Weekly'
	}, {
	    value: 'monthly',
	    title: 'Monthly'
	}, {
	    value: 'yearly',
	    title: 'Yearly'
	}, {
	    value: 'never',
	    title: 'Never'
	}];
	   
	$scope.addLink = addLink;
	$scope.cancel = cancel;
	$scope.ctrl = ctrl;
});

'use strict';
angular.module('ngMaterialDashboardSeo')

/**
 * @ngdoc controller
 * @name AmdSeoLinkCtrl
 * @description 
 * 
 * # AmdSeoLinkCtrl Controller of the ngMaterialDashboardSeo
 */
.controller('AmdSeoLinkCtrl', function ($scope, $seo, $routeParams, $navigator) {

	var ctrl = {
		working: false
	};

	function remove() {
		confirm('Link will be deleted. There is no undo action.')//
		.then(function(){
			ctrl.working = true;
			return $scope.link.delete()//
			.then(function(){
                $navigator.openPage('seo/links');
            }, function(){
                alert('fail to delete link');
            });
		})//
		.finally(function(){
			ctrl.working = false;
		});
	}


	function load() {
		ctrl.working = true;
		return $seo.getLink($routeParams.id)//
		.then(function(link){
			$scope.link = link;
		}, function(){
			alert('Failed to load links');
		}).finally(function(){
		    ctrl.working = false;
		});
	}

	$scope.remove = remove;
	$scope.ctrl = ctrl;
	load();
});
'use strict';
angular.module('ngMaterialDashboardSeo')

/**
 * @ngdoc controller
 * @name AmdSeoLinksCtrl
 * @description Manages view of SEO links
 */
.controller('AmdSeoLinksCtrl', function ($scope, $navigator, $seo, QueryParameter, $controller) {

    // Extends with ItemsController
    angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
        $scope: $scope
    }));

    /*
     * Overried the function
     */
    this.getModelSchema = function () {
        return $seo.linkSchema();
    };

    // get seo links
    this.getModels = function (parameterQuery) {
        return $seo.getLinks(parameterQuery);
    };

    // get a seo link
    this.getModel = function (id) {
        return $seo.getLink(id);
    };

    // delete seo link
    this.deleteModel = function (item) {
        return item.delete();
    };

    this.init({
        eventType: '/seo/links'
    });

    /**
     * To add a new link
     * 
     * @returns
     */
    this.addLink = function () {
        $navigator.openPage('seo/links/new');
    };

    this.sortKeys = [
        'title',
        'description',
        'loc',
        'lastmod',
        'priority'
        ];

    var ctrl = this;
    this.addActions([{
        title: 'New link',
        icon: 'add',
        action: function () {
            ctrl.addLink();
        }
    }]);
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardSeo')
/*
 * Addig syste settings
 */
.run(function($navigator) {
    $navigator.newGroup({
        id: 'seo',
        title: 'SEO',
        icon: 'search',
        priority : 5,
        hidden: '!app.user.tenant_owner'
    });
});
///* 
// * The MIT License (MIT)
// * 
// * Copyright (c) 2016 weburger
// * 
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// * 
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// * 
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//'use strict';
//
//angular.module('ngMaterialDashboardSeo')
//
///**
// * Load widgets
// */
//.run(function ($settings) {
//    $settings.addPage({
//        type: 'alexa-rank',
//        label: 'Alexa rank',
//        description: 'Show the rank of site using alexa.',
//        icon: 'settings',
//        templateUrl: 'views/settings/alexa-rank.html',
//        controller: function () {
//            this.urlChanged = function () {
//                this.setProperty('url', this.url);
//            };
//            this.init = function () {
//                this.url = this.getProperty('url');
//            };
//        },
//        controllerAs: 'ctrl'
//    });
//});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardSeo')
	/*
	 * Register modules
	 */
	.run(function ($widget) {

	    $widget.newWidget({
		type: 'Alexa',
		title: 'Alexa rank',
		description: 'Show the rank of site using alexa.',
		groups: ['commons'],
		icon: 'web',
		// help
		help: '',
		helpId: '',
		model: {
		    style: {
			size: {
			    width: '340px',
			    height: '150px'
			}
		    }
		},
		// functional (page)
		templateUrl: 'views/widgets/alexa-rank.html',
		setting: ['common-features', 'alexa-rank'],
		controller: function () {
		    this.initWidget = function () {
			var ctrl = this;
			this.url = this.getModelProperty('url');
			this.on('modelUpdated', function (event) {
			    if (event.key === 'url') {
				ctrl.url = ctrl.getModelProperty('url');
			    }
			});
		    };
		},
		controllerAs: 'ctrl'
	    });
	}
	);
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngMaterialDashboardSeo')

/**
 * @ngdoc Services
 * @name $actions
 * @description Manage application actions
 * 
 * Controllers and views can access actions which is registered by an
 * applications. This service is responsible to manage global actions.
 * 
 */
.service('$seoContent', function($q) {

    /**
     * Remove extra informations and optimized the page
     * 
     * options:
     * 
     * removeStyle:
     * 
     * removeAngularjsTags:
     * 
     * removeAngularjsMDTags:
     * 
     */
    this.optimize = function(htmlText, options) {
        var content = htmlText;
        // TODO: maso, 2019: check options

        if(options.removeStyle){
            content = this.removeStyle(content);
        }
        if(options.removeAngularjsTags){
            content = this.removeAngularJSTags(content);
        }
        if(options.removeAngularjsMDTags){
            content = this.removeAngularJSMaterialTags(content);
        }
        content = this.removeAngularJSMBTags(content);
        return $q.resolve(content);
    };

    this.removePatterns = function(content, patterns) {
        var matches = null;
        for(let j = 0; j < patterns.length; j++){
            matches = content.match(patterns[j]);
            for (let i = 0; matches && i < matches.length; i++) {
                content = content.replace(matches[i], '');
            }
        }
        return content;
    };

    this.removeStyle = function(htmlText) {
        var patterns = [
            /<style(?:.*?)>(?:[\S\s]*?)<\/style>/gim,

            / class=\"(?:[^\"]*?)\"/gi,
            / style=\"(?:[^\"]*?)\"/gim,
            / draggable=\"(?:[^\"]*?)\"/gim,
            / color=\"(?:[^\"]*?)\"/gim,
            / tabindex=\"(?:[^\"]*?)\"/gim,
            / width=\"(?:[^\"]*?)\"/gim,
            / height=\"(?:[^\"]*?)\"/gim,
            / size=\"(?:[^\"]*?)\"/gim,
            / align=\"(?:[^\"]*?)\"/gim,
            ];
        return this.removePatterns(htmlText, patterns);
    };

    this.removeAngularJSTags = function(htmlText){
        var patterns = [
            // move angular
            /(\s)+ng-(?:[^=]+)=\"(?:[^\"]*)\"/gim,
            /(\s)+data-ng-(?:[^=]+)=\"(?:[^\"]*)\"/gim,
            /(\s)+transclude=\"(?:[^\"]*)\"/gim,
            /<!--(\s)*ng(?:\w+)\:((?!-->).)*-->/gi,
            /<!--(\s)*end ng(?:\w+)\:((?!-->).)*-->/gi,
            ];
        return this.removePatterns(htmlText, patterns);
    };

    this.removeAngularJSMaterialTags = function(htmlText){
        var patterns = [
            // move angular-material design
            /(\s)+md-(?:[^=]+)=\"(?:[^\"]*)\"/gim,
            /(\s)+data-md-(?:[^=]+)=\"(?:[^\"]*)\"/gim,
            /(\s)+layout=\"(?:[^\"]*)\"/gim,
            /(\s)+layout-(?:[^=]+)=\"(?:[^\"]*)\"/gim,
            /(\s)+flex=\"(?:[^\"]*)\"/gim,
            /(\s)+flex-(?:[^=]+)=\"(?:[^\"]*)\"/gim,

            // TODO: maso: may application
            /(\s)+wb-(?:[^=]+)=\"(?:[^\"]*)\"/gim,
            /(\s)+data-wb-(?:[^=]+)=\"(?:[^\"]*)\"/gim,
            /(\s)+translate=\"(?:[^\"]*)\"/gim,

            /(\s)+dnd-(?:[^=]+)=\"(?:[^\"]*)\"/gim,
            /(\s)+data-dnd-(?:[^=]+)=\"(?:[^\"]*)\"/gim,

            /(\s)+ui-(?:[^=]+)=\"(?:[^\"]*)\"/gim,
            /(\s)+data-ui-(?:[^=]+)=\"(?:[^\"]*)\"/gim,
            ];

        return this.removePatterns(htmlText, patterns);
    };

    this.removeAngularJSMBTags = function(htmlText){
        var patterns = [
            // TODO: maso: may application
            /(\s)+wb-(?:[^=]+)=\"(?:[^\"]*)\"/gim,
            /(\s)+data-wb-(?:[^=]+)=\"(?:[^\"]*)\"/gim,
            /(\s)+translate=\"(?:[^\"]*)\"/gim,

            /(\s)+dnd-(?:[^=]+)=\"(?:[^\"]*)\"/gim,
            /(\s)+data-dnd-(?:[^=]+)=\"(?:[^\"]*)\"/gim,

            /(\s)+ui-(?:[^=]+)=\"(?:[^\"]*)\"/gim,
            /(\s)+data-ui-(?:[^=]+)=\"(?:[^\"]*)\"/gim,
            ];

        return this.removePatterns(htmlText, patterns);
    };

    return this;
});
angular.module('ngMaterialDashboardSeo').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/amd-seo-backend-new.html',
    "<md-content mb-preloading=creatingNewGate layout=column layout-padding flex> <div layout=column layout-align=\"center center\" ng-cloak> <p translate>Select an engine from the following list:</p> <div layout=column layout-align=\"center center\"> <md-select required md-no-asterisk=false placeholder=\"Render engine\" ng-model=_engine md-on-open=loadEngines() style=\"min-width: 200px\"> <md-option ng-value=engine ng-repeat=\"engine in engines.items\">{{engine.title}}</md-option> </md-select> </div> </div> <div layout=column ng-show=properties> <h3>{{selectedEngine.title}}</h3> <p>{{selectedEngine.description}}</p> <md-input-container ng-repeat=\"property in properties\"> <label ng-if=\"property.type !== 'Boolean'\">{{property.title}}</label> <input ng-if=\"property.type !== 'Boolean'\" ng-model=_userValus[property.name]> <md-switch ng-if=\"property.type === 'Boolean'\" ng-model=_userValus[property.name]> {{property.title}} </md-switch> </md-input-container> <div layout=row> <span flex></span> <md-button class=\"md-primary md-raised\" ng-click=newBackend(_engine,_userValus)> <span translate>Create</span> </md-button> <md-button ng-href=settings/seo-backends> <span translate>Cancel</span> </md-button> </div> </div> </md-content>"
  );


  $templateCache.put('views/amd-seo-backend.html',
    "<md-content layout=column layout-padding mb-preloading=loadingBackend flex> <div> <div layout=row layout-padding> <am-wb-inline ng-model=backend.symbol am-wb-inline-type=image am-wb-inline-label=\"Backend name\" am-wb-inline-enable=app.user.tenant_owner am-wb-inline-on-save=save()> <img height=64px src={{backend.symbol}} alt=\"{{backend.title}}\"> </am-wb-inline> <div> <div layout=column> <am-wb-inline ng-model=backend.title am-wb-inline-type=text am-wb-inline-label=\"Backend name\" am-wb-inline-enable=app.user.tenant_owner am-wb-inline-on-save=save()> {{backend.title}}  </am-wb-inline> <am-wb-inline ng-model=backend.description am-wb-inline-type=text am-wb-inline-label=\"Backend name\" am-wb-inline-enable=app.user.tenant_owner am-wb-inline-on-save=save()> {{backend.description}} </am-wb-inline> </div> </div> </div> </div> <md-switch ng-change=save() ng-model=backend.enable aria-label=Enable> <span translate>Enable backend</span> </md-switch> <div layout=row> <span flex></span> <md-button class=md-raised ng-href=seo/backends> <span translate>Cancel</span> </md-button> <md-button class=\"md-raised md-accent\" ng-click=remove()> <span translate>Delete</span> </md-button> </div> </md-content>"
  );


  $templateCache.put('views/amd-seo-backends.html',
    "<div layout=column flex> <mb-pagination-bar mb-model=ctrl.queryParameter mb-properties=ctrl.properties mb-reload=ctrl.reload() mb-more-actions=ctrl.getActions()> </mb-pagination-bar> <md-content mb-preloading=\"ctrl.state === 'busy'\" mb-infinate-scroll=ctrl.loadNextPage() layout=column flex> <md-list flex> <md-list-item ng-href=seo/backends/{{object.id}} ng-repeat=\"object in ctrl.items track by object.id\" class=md-3-line> <img ng-if=object.symbol ng-src={{object.symbol}} class=md-avatar alt={{object.id}}> <wb-icon ng-if=!object.symbol>dvr</wb-icon> <div class=md-list-item-text layout=column> <h3>{{object.title}}</h3> <h4>{{object.engine}}</h4> <p>{{object.description}}</p> </div> <md-button class=md-icon-button ng-click=\"ctrl.deleteItem(object, $event)\"> <wb-icon>delete</wb-icon> </md-button> <md-divider md-inset></md-divider> </md-list-item> </md-list> <div layout=column layout-align=\"center center\" ng-show=\"ctrl.state !== 'busy' && ctrl.items && ctrl.items.length === 0\"> <h2 translate=\"\">Empty backend list</h2> <p translate=\"\">Any backend match with the query. You can add a new backend by click on the add button. Links improve your site rank.</p> <md-button ng-click=ctrl.addItem($event)> <wb-icon>add</wb-icon> <span translate=\"\">Add</span> </md-button> </div> </md-content> </div>"
  );


  $templateCache.put('views/amd-seo-content-render.html',
    "<md-content id=amd-seo-render-view ng-controller=\"AmdSeoContentRenderCtrl as ctrl\" flex> <mb-titled-block id=option layout=column mb-title=Options mb-icon=settings> <div layout=column> <md-checkbox ng-model=options.removeStyle aria-label=\"Remove style from view\"> <span translate>Remove style attribute</span> </md-checkbox> <md-checkbox ng-model=options.removeAngularjsTags aria-label=\"Remove AngularJS attributes from view\"> <span translate>Remove AngularJS attributes</span> </md-checkbox> <md-checkbox ng-model=options.removeAngularjsMDTags aria-label=\"Remove AngularJS Material Desing attributes from view\"> <span translate>Remove AngularJS Material Design attributes</span> </md-checkbox> </div> <div layout=row> <span flex></span> <md-button class=md-raised ng-href=/api/v2/seo/content/{{ctrl.seoContent.id}}/content target=_blank download={{ctrl.seoContent.file_name}}> <span translate>Download</span> </md-button> <md-button class=\"md-raised md-primary\" ng-click=ctrl.uploadPreview(options)> <span translate>Upload preview</span> </md-button> </div> </mb-titled-block> <mb-titled-block id=info layout=column mb-title=Information mb-icon=info> <table> <tr> <td translate>URL</td> <td>{{ctrl.seoContent.url}}</td> </tr> <tr> <td translate>Title</td> <td>{{ctrl.seoContent.title}}</td> </tr> <tr> <td translate>description</td> <td>{{ctrl.seoContent.description}}</td> </tr> <tr> <td translate>mime_type</td> <td>{{ctrl.seoContent.mime_type}}</td> </tr> <tr> <td translate>file_name</td> <td>{{ctrl.seoContent.file_name}}</td> </tr> <tr> <td translate>file_size</td> <td>{{ctrl.seoContent.file_size}}</td> </tr> <tr> <td translate>downloads</td> <td>{{ctrl.seoContent.downloads}}</td> </tr> <tr> <td translate>creation_dtime</td> <td>{{ctrl.seoContent.creation_dtime}}</td> </tr> <tr> <td translate>modif_dtime</td> <td>{{ctrl.seoContent.modif_dtime}}</td> </tr> <tr> <td translate>expire_dtime</td> <td> <mb-inline ng-model=ctrl.seoContent.expire_dtime mb-inline-type=date mb-inline-enable=editable mb-inline-on-save=ctrl.saveContent()> {{ (ctrl.seoContent.expire_dtime | mbDate) || 'empty' }}  </mb-inline></td> </tr> </table> </mb-titled-block> <mb-titled-block id=preview layout=column mb-title=\"Live preview\" mb-icon=visibility> <div id=url> {{ctrl.seoContent.url}} </div> <iframe style=\"width: 90%\" ng-src={{ctrl.currentContenttUrl}} height=600> IFrame is not supported by your browser </iframe> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/amd-seo-crawled-links.html',
    "<div layout=column flex> <mb-pagination-bar mb-model=ctrl.queryParameter mb-properties=ctrl.properties mb-reload=ctrl.reload() mb-more-actions=ctrl.getActions()> </mb-pagination-bar> <md-content mb-preloading=\"ctrl.state === 'busy'\" mb-infinate-scroll=ctrl.loadNextPage() layout=column flex> <md-list> <md-list-item ng-repeat=\"object in ctrl.items track by object.id\" ng-href=seo/crawled-links/{{object.id}}/render class=md-3-line> <wb-icon>donut_small</wb-icon> <div class=md-list-item-text layout=column> <h3>{{object.title}}</h3> <h4>{{object.url}}</h4> <p>{{object.description}}</p> </div> <md-button class=md-icon-button ng-click=ctrl.upload(object) aria-label=\"Upload content\"> <wb-icon>cloud_upload</wb-icon> </md-button> <md-button class=md-icon-button aria-label=\"Download content\" ng-href=/api/v2/seo/contents/{{object.id}}/content target=_blank download={{object.file_name}}> <wb-icon>cloud_download</wb-icon> </md-button> <md-button class=md-icon-button ng-href={{object.url}} aria-label=\"Render content\"> <wb-icon>open_in_browser</wb-icon> </md-button> <md-button class=md-icon-button ng-click=ctrl.deleteItem(object) aria-label=\"Delete content\"> <wb-icon>delete</wb-icon> </md-button> <md-divider md-inset></md-divider> </md-list-item> </md-list> <div layout=column layout-align=\"center center\" ng-show=\"ctrl.state !== 'busy' && ctrl.items && ctrl.items.length === 0\"> <h2 translate>Empty crawled links</h2> <p translate>There is no link in crawled list. This means that no crawler has requested your site.</p> </div> </md-content> </div>"
  );


  $templateCache.put('views/amd-seo-engines.html',
    "Engines"
  );


  $templateCache.put('views/amd-seo-link-new.html',
    "<md-content class=md-padding layout-padding flex> <form name=linkForm ng-submit=addLink(model) mb-preloading=ctrl.saving layout-margin layout=column flex> <md-input-container> <label translate>Title</label> <input ng-model=model.title> </md-input-container> <md-input-container> <label translate>Description</label> <input ng-model=model.description> </md-input-container> <md-input-container> <label translate>Link (loc)</label> <input name=loc ng-model=model.loc type=url required> <div ng-messages=linkForm.loc.$error> <div ng-message=required>{{'This field is required.' | translate}}</div> <div ng-message=url>{{'URL is not valid.' | translate}}</div> </div> </md-input-container> <md-input-container> <label translate>Last Modification</label> <md-datepicker ng-model=myDate md-hide-icons=calendar ng-change=\"model.lastmod = formatDate(myDate)\"></md-datepicker> </md-input-container> <md-input-container> <label translate>Change Frequency</label> <md-select ng-model=model.changefreq> <md-option ng-repeat=\"item in changeFrequencies\" value={{item.value}}> {{item.title}} </md-option> </md-select> </md-input-container> <md-input-container> <label translate>Priority</label> <input name=priority type=number ng-model=model.priority min=0.0 max=1.0 step=0.01> <div ng-messages=linkForm.priority.$error> <div ng-message=min>{{'Minimum value is 0.0.' | translate}}</div> <div ng-message=max>{{'Maximum value is 1.0.' | translate}}</div> </div> </md-input-container> <div layout=row> <span flex></span> <md-button class=md-raised ng-click=cancel()> <wb-icon aria-label=close>close</wb-icon> <span translate>Cancle</span> </md-button> <md-button class=md-raised ng-disabled=linkForm.$invalid ng-click=addLink(model)> <wb-icon aria-label=close>done</wb-icon> <span translate>Add</span> </md-button> </div> </form> </md-content>"
  );


  $templateCache.put('views/amd-seo-link.html',
    "<md-content layout=column layout-padding flex> <table> <tr><td translate>Title: </td><td>{{link.title}}</td></tr> <tr><td translate>Link: </td><td><a ng-href={{link.loc}}>{{link.loc}}</a></td></tr> <tr><td translate>Priority: </td><td>{{link.priority}}</td></tr> <tr><td translate>Last modification: </td><td>{{link.lastmod}}</td></tr> <tr><td translate>Description:</td><td>{{link.description}}</td></tr>  </table> <div layout=row> <span flex></span> <md-button class=\"md-raised md-accent\" ng-click=remove()> <wb-icon>delete</wb-icon> <span translate>delete</span> </md-button> </div> </md-content>"
  );


  $templateCache.put('views/amd-seo-links.html',
    "<div mb-preloading=ctrl.loading layout=column flex> <mb-pagination-bar mb-model=ctrl.queryParameter mb-properties=ctrl.properties mb-reload=ctrl.reload() mb-more-actions=ctrl.getActions()> </mb-pagination-bar> <md-content mb-preloading=\"ctrl.state==='busy'\" mb-infinate-scroll=ctrl.loadNextPage() layout=column flex> <md-list> <md-list-item ng-show=\"!ctrl.loadingItems && ctrl.items.length\" ng-repeat=\"object in ctrl.items track by object.id\" ng-href=seo/links/{{object.id}} class=md-3-line> <wb-icon>link</wb-icon> <div class=md-list-item-text layout=column> <h3><strong>{{object.title}}</strong> [<a href={{object.loc}}>{{object.loc}}</a>]</h3> <h4>{{'Priority' | translate}}: <b>{{object.priority}}</b> - {{'Last modification' | translate}}: {{object.lastmod}}</h4> <p>{{object.description}}</p> </div> <md-button class=md-icon-button ng-click=ctrl.deleteItem(object) aria-label=\"remove attachment\"><wb-icon>delete</wb-icon> </md-button> <md-divider md-inset></md-divider> </md-list-item> </md-list> <div layout=column layout-align=\"center center\" ng-if=\"ctrl.state !== 'busy' && ctrl.items && !ctrl.items.length\"> <h2>Empty link list</h2> <md-button class=md-raised ng-click=addLink()> <wb-icon>add</wb-icon> <span translate>New link</span> </md-button> </div> </md-content> </div>"
  );


  $templateCache.put('views/dialogs/select-file.html',
    "<md-dialog aria-label=\"Select File\" ng-cloak> <md-toolbar> <div class=md-toolbar-tools> <h2 translate=\"\">Select File</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config)> <wb-icon aria-label=Done>done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=close>close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout=column layout-padding> <form name=contentForm ng-action=ctrl.upload(config) mb-preloading=ctrl.uploading layout-margin layout=column flex> <md-input-container> <lf-ng-md-file-input lf-files=config.files progress preview drag required></lf-ng-md-file-input> <div ng-messages=!lf-files.file[0]> <div ng-message=required>This is required!</div> </div> </md-input-container> </form> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/settings/alexa-rank.html',
    "<div layout-padding layout=column> <md-input-container> <label translate=\"\">URL of site</label> <input ng-model=ctrl.url ng-change=ctrl.urlChanged() ng-model-options=\"{updateOn: 'change blur'}\"> </md-input-container> </div>"
  );


  $templateCache.put('views/sitemap.html',
    "<md-content layout=column flex> <iframe height=100% id=sitemap src=/api/v2/seo/sitemap/sitemap.xml></iframe> </md-content>"
  );


  $templateCache.put('views/widgets/alexa-rank.html',
    "<a ng-if=ctrl.editable> <img width=100% height=100% ng-src=\"https://traffic.alexa.com/graph?o=lt&y=t&b=ffffff&n=666666&f=999999&p=4e8cff&r=1y&t=2&z=30&c=1&h=150&w=340&u={{ctrl.url}}\"> </a> <a ng-if=!ctrl.editable ng-href=https://www.alexa.com/siteinfo/{{ctrl.url}}> <img width=100% height=100% ng-src=\"https://traffic.alexa.com/graph?o=lt&y=t&b=ffffff&n=666666&f=999999&p=4e8cff&r=1y&t=2&z=30&c=1&h=150&w=340&u={{ctrl.url}}\"> </a>"
  );

}]);
